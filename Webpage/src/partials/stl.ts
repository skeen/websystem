/// <reference path="../../res/CuraEngine/worker/CuraEngineWorker.ts" />

module STL
{
    var engine : ICuraEngine;
    
    export function main() : void
    {
        var data = localStorage["stl"];
        (<HTMLTextAreaElement> document.getElementById('stl_content')).value = data;

        document.getElementById('slice').addEventListener('click', function(event)
        {
            slice_file();
        });
        alert("stl main called");

        load_engine();
    }

    function load_engine() : void
    {
        if(engine != null) 
        {
            return;
        }

        engine = new CuraEngineWorker(function()
        {
            engine.load(function () {
                document.getElementById('slice').disabled = false;
            });
        }, "js/CuraEngine/CuraEngineWebWorker.js");
        alert("load engine called");
    }

    function slice_file() : void
    {
        var data = localStorage["stl"];
        engine.save_ascii_model(data, function() {
            engine.run(function() {
            console.log("Main called!");
            console.log("Printing stderr:");
            console.log(engine.get_stderr());
            console.log("Printing stdout:");
            console.log(engine.get_stdout());
            console.log("Printing status:");
            console.log(engine.get_status());
            console.log("Printing gcode:");
            var gcode = engine.get_gcode();
            console.log(gcode);

            localStorage["gcode"] = gcode;
            location.href = "index.html#/GCODE";
        });
        });
    }

    console.log("loaded stl.ts!");
}

var app;
app.registerCtrl('stlController', function($scope)
{
     $scope.pageClass = 'page-stl';

     console.log("main_stl called");
     STL.main();
});
