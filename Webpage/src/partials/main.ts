module Main
{
    export function main() : void
    {
        if(browser_fulfills_website_requirements() === false)
        {
            alert("insufficient browser support");
            return;
        }
        alert("main called");

        console.log("main actually called");

        document.getElementById('upload').addEventListener('change', handle_file, false);
    }

    // Check if we support html5 storage
    function supports_html5_storage() : boolean
    {
        return (window.localStorage != null);
    }

    // Check if we support html5 fileAPI
    /*
    supports_html5_fileapi() : boolean
    {
        return (window.File != null && window.FileReader != null && window.FileList != null && window.Blob != null);
    }
    */

    function browser_fulfills_website_requirements() : boolean
    {
        return supports_html5_storage();
      //         supports_html5_fileapi();
    }

    function load_stl(text : String) : void
    {
        alert('load_stl');
        alert(text);
        localStorage["stl"] = text;
    }

    function load_gcode(text : String) : void
    {
        alert('load_gcode');
        alert(text);
        localStorage["gcode"] = text;
    }

    function handle_file(evt) : void
    {
        if (!evt.target.files.length) 
        {
            alert('Please select a file!');
            return;
        }
        var file = evt.target.files[0];
        var file_extension = file.name.split('.').pop();

        var reader = new FileReader();

        if(file_extension == "stl")
        {
            alert("stl file!");
            reader.onload = function(e) {
                load_stl(reader.result);
            };
            reader.readAsText(file);

            location.href = "index.html#/STL";
        }
        else if(file_extension == "gcode")
        {
            alert("gcode file!");
            reader.onload = function(e) {
                load_gcode(reader.result);
            };
            reader.readAsText(file);

            location.href = "index.html#/GCODE";
        }
        else
        {
            alert("Unknown file extension: " + file_extension);
        }
    }

    console.log("loaded main.ts!");
}

var app;
app.registerCtrl('mainController', function($scope)
{
     $scope.pageClass = 'page-main';

     alert("mainController call");

     console.log("main_main called");
     Main.main();
});
