// Holds the machine server ip
var machine_server;

module GCode
{
    var http;

    export function main($http) : void
    {
        http = $http;

        var data = localStorage["gcode"];
        (<HTMLTextAreaElement> document.getElementById('gcode_content')).value = data;

        document.getElementById('upload').addEventListener('click', function(event)
        {
            upload_gcode();
        });
 
    }

    function upload_gcode() : void
    {
        var dataz = localStorage["gcode"];
        var filename = (<HTMLInputElement> document.getElementById('upload_filename')).value;

        alert(filename);
 
        alert("machine server");
        alert(machine_server);

        var responsePromise = http.get("http://" + machine_server + "/gcode?gcode=M28+" + filename);
        responsePromise.success(function(data, status, headers, config)
        {
            var lines = dataz.split('\n');
            var start = function recursive(i)
            {
                if( i == lines.length )
                {
                    var responsePromise = http.get("http://" + machine_server + "/gcode?gcode=M29+" + filename);
                    responsePromise.success(function(data, status, headers, config)
                    {
                        alert("SUB-AJAX!");
                        alert(data);
                    });
                    return;
                }

                var line = lines[i];
                var responsePromise = http.get("http://" + machine_server + "/gcode?gcode=" + line);
                responsePromise.success(function(data, status, headers, config)
                {
                    (<HTMLTextAreaElement> document.getElementById('progress')).value = "line:" + i + "/" + lines.length;


                    recursive(i+1);
                });
                responsePromise.error(function(data, status, headers, config)
                {
                    alert("AJAX failed!");
                    alert("i " + i);
                });
            }

            start(0);

            alert("AJAX succes!");
            alert(data);
        });
        responsePromise.error(function(data, status, headers, config)
        {
            alert("AJAX failed!");
        });
    }

    console.log("loaded gcode.ts!");
}

var app;
app.registerCtrl('gcodeController', function($scope, $http)
{
     $scope.pageClass = 'page-gcode';

     console.log("main_gcode called");
     GCode.main($http);
});
