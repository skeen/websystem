// Holds the machine server ip
var machine_server;

module Control
{
    var http;

    export function main($http) : void
    {
        http = $http;

        document.getElementById('control-up').addEventListener('click', function(event)
        {
            handle_keypress("Y", 1);
        });
        document.getElementById('control-down').addEventListener('click', function(event)
        {
            handle_keypress("Y", -1);
        });
        document.getElementById('control-right').addEventListener('click', function(event)
        {
            handle_keypress("X", 1);
        });
        document.getElementById('control-left').addEventListener('click', function(event)
        {
            handle_keypress("X", -1);
        });
        document.getElementById('control-home-xy').addEventListener('click', function(event)
        {
            home("X+Y");
        });

        document.getElementById('control-up-z').addEventListener('click', function(event)
        {
            handle_keypress("Z", 1);
        });
        document.getElementById('control-down-z').addEventListener('click', function(event)
        {
            handle_keypress("Z", -1);
        });
        document.getElementById('control-home-z').addEventListener('click', function(event)
        {
            home("Z");
        });

        document.getElementById('control-extrude').addEventListener('click', function(event)
        {
            var move_length = parseInt((<HTMLInputElement> document.getElementById('control-mm')).value);
            handle_movement("E", move_length);
        });
        document.getElementById('control-retract').addEventListener('click', function(event)
        {
            var move_length = parseInt((<HTMLInputElement> document.getElementById('control-mm')).value);
            handle_movement("E", -move_length);
        });

        document.getElementById('control-motors-off').addEventListener('click', function(event)
        {
            motors_off();
        });
    }

    function move_length() : number
    {
        var checked_01 = (<HTMLInputElement> document.getElementById('length-01')).checked;
        var checked_1 = (<HTMLInputElement> document.getElementById('length-1')).checked;
        var checked_10 = (<HTMLInputElement> document.getElementById('length-10')).checked;
        var checked_100 = (<HTMLInputElement> document.getElementById('length-100')).checked;
      
        if(checked_01 === true)
            return 0.1;
        else if(checked_1 === true)
            return 1;
        else if(checked_10 === true)
            return 10;
        else if(checked_100 === true)
            return 100;
        else
        {
            alert("Something went terribly wrong!");
            return 0;
        }
    }

    function motors_off() : void
    {
        var responsePromise = http.get("http://" + machine_server + "/gcode?gcode=M18");
        responsePromise.success(function(data, status, headers, config)
        {
            alert("Motors off");
            alert(data);
        });
        responsePromise.error(function(data, status, headers, config)
        {
            alert("AJAX failed!");
        });

    }

    function home(axis : string) : void
    {
        var responsePromise = http.get("http://" + machine_server + "/gcode?gcode=G28+" + axis);
        responsePromise.success(function(data, status, headers, config)
        {
            alert("Home sent");
            alert(data);
        });
        responsePromise.error(function(data, status, headers, config)
        {
            alert("AJAX failed!");
        });
    }

    function handle_keypress(axis : string, direction : number) : void
    {
        var length = move_length();
        handle_movement(axis, length * direction);
    }

    function handle_movement(axis : string, length : number) : void
    {
        alert("http://" + machine_server + "/gcode?gcode=G1+" + axis + length);

        var responsePromise = http.get("http://" + machine_server + "/gcode?gcode=G1+" + axis + length);
        responsePromise.success(function(data, status, headers, config)
        {
            alert("Movement sent");
            alert(data);
        });
        responsePromise.error(function(data, status, headers, config)
        {
            alert("AJAX failed!");
        });
    }

    console.log("loaded control.ts!");
}

var app;
app.registerCtrl('controlController', function($scope, $http)
{
     $scope.pageClass = 'page-control';

     console.log("main_control called");
     Control.main($http);
});
